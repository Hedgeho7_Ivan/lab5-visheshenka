/*!
\file TTDTT.cpp
\brief ����, ���������� ������� ������� ���������
*/

/*!
\mainpage 
\brief ��������� ��� ����������� ������, �������������� � ������� ��������������� ����� �� �N� ��������, � ������ ��������� �N� ��� ������ ������.
*\author ����������� �.�.

��������� ��������� ��������� �������:
<ol>
<li>��������� ����� �� ������� ������������ ��������;</li>
<li>���������� ������ �����(�) �� ���������� ������� � ������;</li>
<li>�������������� ������(�);</li>
<li>���������� � ���� ������������ �����;</li>
<li>�������� �� ������ � ������, ����� ������� �����������;</li>
<li>�������� �� ������ � ������, ����� ���� ����;</li>
</ol>
<p>��������� ��������� �� ���� �����, ������� ���������� ������������.</p>
<p>��������� ���������� �������������� �����.</p>
*/

#include "stdafx.h"
#include "TTDTT_Function.h"

/*!������� ������� �������
*\param [in] argc - ���������� ���������� ����������
*\param [in] argv - ���������� ���������(���� � ������)
*\return - 0 ��� ���������� ������ ���������

*\brief ������� ������� �������

������� ���������� ���� � ����� ������ � ����� �������, �������� ������� ������
*/
int main(int argc, char *argv[])
{
	int i = 0;
	int currentString = 0;	//���������� ����� � ������ �����
	int lengthText = 0;		//����� ������
	int errorFlag = 0;		//���� ������
								//errorFlag = 1 - ���� �� ������
								//errorFlag = 2 - ���� �� �����������
								//errorFlag = 3 - ���� - ����
								//errorFlag = 4 - � ������ ���� ������������ �������
								//errorFlag = 5 - � ����� ������ N - ������������ ������
	int kolvoFlag = 0;		//����� ������ ������������ � ������ ������
	char suffix[] = "_deciphered.";

	setlocale(LC_ALL, "Russian");
	//��������� �������� ����� � �������

	if (argc > 1)						// ���� ������� ����
	{
		char *createOutputFile = new char[strlen(argv[1])+strlen(suffix)];	//�������� ��������� �����
		char *expansion = new char[strlen(argv[1])+strlen(suffix)];			//���������� ��� �������� ���������� �����
		ifstream text(argv[1]);			// ��������� ���������� �����

		if (!text.good())				// ���� ���� �� �������� - ������
		{
			printf("�� ������� ������� ���� %s %c",argv[1],'\n');
			errorFlag = 2;
		}
		else							// ����� ���������� ������
		{
			preparationNameOfTheOutputFile (createOutputFile, expansion, argv); // ������� ��� �������� �������� ��������� �����
			
			std::ifstream file(argv[1], std::ios_base::binary); // ������ ���-�� ���� � ������ ��� ������� ������� ������ � ������� ���������� ������ �� ������
			long nFileLen = 0;
			if (file)
			{
				file.seekg(0, std::ios_base::end);
				nFileLen = file.tellg();
				file.close();
			}

			char *textString = new char[nFileLen+2];		//������ � ������� ���������� ������ �� ������
			char *copyString = new char[nFileLen+2];		//����� ������ ��� � ������������� � ������ � ����

			while(!text.eof() && errorFlag == 0)			// ���� ����� �� �������� ��� �� ���������� ������������ ������
			{
				text.getline(textString,'/n');				// ���������� ������
				currentString++;							// ���������� ����� ������
				errorFlag = checkForInvalidCharacters (currentString, textString); // ����������� ����� ������ ��� ������
				lengthText = lengthText + strlen(textString);			// ����������� ������ ������
			}

			if (lengthText == 0 && errorFlag == 0)						// ���� ����� ��� � ��� ������
			{errorFlag = 3; printf("���� %s %s",argv[1]," ����\n");}	// ���������, ��� ���� ����

			text.clear();
			text.seekg(0, ios_base::beg);					// ���������� ����� ��� ���������� ���������

			currentString = 0;

			while(!text.eof() && errorFlag == 0)			// ���� ����� �� �������� ��� �� ���������� ������������ ������
			{
				text.getline(textString,'/n');				// ���������� ������
				currentString++;							// ���������� ����� ������
				errorFlag = checkNSymbols (currentString, textString);	// ����������� ����� ������ ��� ������
			}

			text.clear();
			text.seekg(0, ios_base::beg);					// ���������� ����� ��� ���������� ���������

			while(!text.eof() && errorFlag == 0)
			{																	//���� ���� �� ��������
				kolvoFlag++;													// ��������� endl
				text.getline(textString,'/n');									// ������� ������
				strcpy(copyString,textString);									// �������� ��� ���������
				decipherString(copyString);										// ��������� ������
				if (errorFlag == 0)												// ���� ������ �� ���������
				output (kolvoFlag, currentString, createOutputFile, copyString);// ������ � ����
			}
			text.close();														//��������� ����
			delete textString;
			delete copyString;
		}	
		delete expansion;
		delete createOutputFile;
	}
	else printf("������: ���� �� ������\n");
	errorFlag = 1;
	
    system("pause");
	return 0;
}